Transactions
============

Transaction objects represent a transaction within the CollectID smart contract. Transaction objects have the following properties:

- **txHash** [string] - The hash of the transaction generated by the smart contract.
- **status** [string] - The current status of the transaction.
- **error** [string] - The error message if the transaction failed.

Possibly transaction statuses include:

- *PENDING* - The transaction is still being processed by the smart contract.
- *COMPLETE* - The transaction has succesfully completed.
- *FAILED* - The transaction failed to clear.

API users should be sure to check that a transaction has a "COMPLETE" status before creating another transaction on the same collectible object.

GET transaction/{txHash}/status
-------------------------------

Return the transaction object of a specific transaction hash.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **txHash** [string] - The user ID of the user issuing the request.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
    	 http://backend.collectid.io/api/v1/transaction/0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3e/status

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **txHash** [string] - An echo of the hash of the transaction.
- **status** [string] - The current status of the transaction.
- **error** [string] - The error message of the transaction (this is an empty string if the status is not "FAILED").

Example response:
::

    {
        "txHash": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3e"
        "status": "PENDING",
        "error": ""
    }


.. _cURL: https://curl.haxx.se/