Requests
========

Requests for the transfer of specific collectibles can be issued by any basic user. Request objects have the following properties:

- **id** [integer] - A unique identifier for the request.
- **created_at** [string] - A timestamp indicating the creation time of the request.
- **requester** [integer] - The user ID of the user that issued the request.
- **tagId** [string] - The tag ID of the collectible for which the request was issued.
- **status** [string] - The current status of the request.

Possible request statuses include:

- *OPEN* - The request exists but has not been accepted.
- *PENDING* - The request has been accepted, but is still processing.
- *COMPLETE* - The request has been accepted and succesfully processed.
- *CLOSED* - The request has been explicitly denied or closed due to transfer of ownership.


POST collectible/request
------------------------

Create a new request for a transfer.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **requester** [integer] - The user ID of the user issuing the request.
- **tagId** [string] - The tag ID of the collectible for which to issue the request.
- **secretKey** [string] - Optional. The secret key generated by a new transfer transaction.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "requester": 2,
                "tagId": "0045E54BAB35C82"
                "secretKey": "abcd1234"
            }
         ' http://backend.collectid.io/api/v1/collectible/request

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **id** [integer] - A unique identifier for the request.

Example response:
::

    {
        "id": 1
    }


GET collectible/{tagId}/requests
--------------------------------

Return all the requests for a specific collectible. If the "secretKey" header is present, the results are filtered to only requests which contain a matching secret key.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **tagId** - The tag ID of the collectible for which to return requests.

Headers
~~~~~~~

- **secretKey** - Optional. The secret key generated by a new transfer transaction.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -H "secretKey: abcd1234" \
         http://backend.collectid.io/api/v1/collectible/0045E54BAB35C82/requests

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **requests** [array] - An array of request objects.

Example response:
::

    {
        "requests": [
            {
                "id": 1,
                "created_at": "2018-09-13T00:00:00.000Z",
                "requester": 2,
                "tag_id": "0045E54BAB35C82",
                "status": "OPEN"
            },
            ...
            {
                "id": 3,
                "created_at": "2018-09-13T00:00:00.000Z",
                "requester": 4,
                "tag_id": "0045E54BAB35C82"
                "status": "PENDING"
            }
        ]
    }


.. _cURL: https://curl.haxx.se/