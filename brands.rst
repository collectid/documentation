Brands
======

Brand objects are mapped to by products and describe the brand using the following fields:

- **id** [integer] - A unique identifier for the brand.
- **name** [string] - A unique name for the brand.
- **detail** [string] - A brief description of the brand.
- **logo** [string] - A URL pointing to the logo image of the brand.
- **creatorId** [integer] - The ID of the user that added the brand to the ecosystem.
- **createdAt** [string] - A timestamp indicating when the brand was created.
- **updatedAt** [string] - A timestamp indicating when the brand was last updated.

Example of a brand object:
::
    {
        "id": 1,
        "name": "Nike",
        "detail": "Just Do It.",
        "logo": "http://images.collectid.io/images/logos/nike_logo.png",
        "creatorId": 42,
        "createdAt": "2018-09-12T00:00:00.000Z",
        "updatedAt": "2018-09-12T00:00:00.000Z"
    }


POST brand
----------

Create a new brand.

Role Required
~~~~~~~~~~~~~

create:brand

Parameters
~~~~~~~~~~

- **creatorId** [integer] - The ID of the user creating the brand.
- **description** [object] - A JSON object containing details of the brand.

A description object has the following properties:

- **name** [string] - The unique name of the brand.
- **detail** [string] - A brief description of the brand.
- **logo** [string] - The URL pointing to the brand logo.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "creatorId": 42,
                "description": {
                    "name": "Nike",
                    "detail": "Just Do It.",
                    "logo": "http://images.collectid.io/images/logos/nike_logo.png"
                }
            }
         ' http://backend.collectid.io/api/v1/brand

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **id** [integer] - A unique identifier for the brand.

Example response:
::
    {
        "id": 1
    }


GET brand
-----------

Retrieve the details of all brands in the ecosystem.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/brand

Role Required
~~~~~~~~~~~~~~

read:brand

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **brands** [array] - An array of brand objects.

Example response:
::
    {
        "brands": [
            {
                "id": 1,
                "name": "Nike",
                "detail": "Just Do It.",
                "logo": "http://images.collectid.io/images/logos/nike_logo.png",
                "creatorId": 42,
                "createdAt": "2018-09-12T00:00:00.000Z",
                "updatedAt": "2018-09-12T00:00:00.000Z"
            },
            ...
            {
                "id": 5,
                "name": "Adidas",
                "detail": "Impossible Is Nothing.",
                "logo": "http://images.collectid.io/images/logos/adidas_logo.png",
                "creatorId": 42,
                "createdAt": "2018-09-12T00:00:00.000Z",
                "updatedAt": "2018-09-12T00:00:00.000Z"
            }
        ]
    }


GET brand/{brandId}
-----------------------

Retrieve the details of a specific brand by ID.

Role Required
~~~~~~~~~~~~~~

read:brand

Parameters
~~~~~~~~~~

- **brandId** - The ID of the brand to be returned.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/brand/1

Response
~~~~~~~~

Returns a JSON object representing a single brand object.

Example response:
::
    {
        "id": 1,
        "name": "Nike",
        "detail": "Just Do It.",
        "logo": "http://images.collectid.io/images/logos/nike_logo.png",
        "creatorId": 42,
        "createdAt": "2018-09-12T00:00:00.000Z",
        "updatedAt": "2018-09-12T00:00:00.000Z"
    }

.. _cURL: https://curl.haxx.se/