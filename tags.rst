Tags
====

Tags are unique item identifiers that are assigned to each collectible in the CollectID ecosystem. Tag objects have the following properties:

- **tag** [string] - A unique identifier.
- **status** [string] - Status of the tag.

Possible tag statuses include:

- *CODED* - The tag is created, but not activated for use in the ecosystem.
- *COMMISSIONED* - The tag is activated for use.
- *DECOMMISSIONED* - The tag has been deactivated for use in the ecosystem.

Example of a tag object:
::

    {
        "tag": "0045E54BAB35C82",
        "status": "CODED"
    }


POST tag
--------

Add a set of tags to the ecosystem.

Role Required
~~~~~~~~~~~~~~

verifier

Parameters
~~~~~~~~~~

- **tags** [string array] - An array of tags represented as strings.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "tags": [
                    "0045E54BAB35C82",
                    "0045E54BAB35C83",
                    "0045E54BAB35C84"
                ]
            }
         ' http://backend.collectid.io/api/v1/tag

Response
~~~~~~~~

A succesful call to this endpoint will simply return a 200 status.


POST tag/decommission
---------------------

Decommission a set of tags from the ecosystem. This will also decommission any collectible associated with the tags.

Role Required
~~~~~~~~~~~~~~

admin

Parameters
~~~~~~~~~~

- **tags** [string array] - An array of tags represented as strings.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "tags": [
                    "0045E54BAB35C82",
                    "0045E54BAB35C83",
                    "0045E54BAB35C84"
                ]
            }
         ' http://backend.collectid.io/api/v1/tag/decommission

Response
~~~~~~~~

A succesful call to this endpoint will simply return a 200 status.


.. _cURL: https://curl.haxx.se/