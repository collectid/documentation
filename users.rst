Users
=====

Users are unique actors in the CollectID ecosystem. Upon creation, each user is assigned one or more roles that control what platform functionality is accessible to them. User objects have the following properties:

- **id** [integer] - A unique identifier.
- **role** [array] - An array of strings representing the set of roles given to the user.
- **address** [string] - The Ethereum wallet address associated with the user.
- **publicKey** [string] - The ECDSA public key associated with the user.
- **externalId** [string] - The external identifier for the user provided by the authenticity server.
- **email** [string] - The email address of the user.
- **name** [string] - The name of the user.
- **username** [string] - The username of the user.

Possible roles include:

- *COLLECTOR* - User is allowed access to collection functionality.
- *BUYER* - User is allowed access to buying functionality.
- *SELLER* - User is allowed access to selling functionality.
- *VERIFIER* - User is allowed access to collectible commissioning and tag generation functionality.
- *ADMIN* - User is allowed access to administrative and platform management functionality.
- *OWNER* - User is allowed full control over smart contract.

.. NOTE::
    A typical "basic" user has COLLECTOR, BUYER, and SELLER roles.

Example user object:
::

    {
        "id": 1
        "role": ["COLLECTOR", "BUYER", "SELLER"],
        "address": "0x0000000000000000000000000000000C011EC71D",
        "publicKey": "0x00000000000000...00000000000000C011EC71D",
        "externalId": "abcd1234",
        "email": "hugh.mann@collectid.io",
        "name": "Hugh Mann",
        "username": "hughmann2019"
    }

POST user
---------

Create a basic new user.

Role Required
~~~~~~~~~~~~~~

write:users

Parameters
~~~~~~~~~~

- **address** [string] - An Ethereum wallet address for the user. Must be unique to the ecosystem.
- **publicKey** [string] - An ECDSA public key for the user. Must be unique to the ecosystem.
- **externalId** [string] - The external identifier for the user provided by the authenticity server. Must be unique to the ecosystem.
- **email** [string] - The email address of the user. Must be unique to the ecosystem.
- **name** [string] - The name of the user.
- **username** [string] - The username of the user. Must be unique to the ecosystem.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "address": "0x0000000000000000000000000000000C011EC71D",
                "publicKey": "0x00000000000000...00000000000000C011EC71D",
                "externalId": "abcd1234",
                "email": "hugh.mann@collectid.io",
                "name": "Hugh Mann",
                "username": "hughmann2019"
            }
         ' http://backend.collectid.io/api/v1/user

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **id** [integer] - The unique identifier for the created user.

Example response:
::

    {
        "id": 1
    }


PUT user/{id}/roles
-------------------

Update the user role of a specific user.

Role Required
~~~~~~~~~~~~~~

edit:users

Parameters
~~~~~~~~~~

- **id** [integer] - The unique identifier for a specific user to be updated.
- **role** [array] - An array of strings representing the set of roles given to the user.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X PUT \
         -d '
            {
                "role": ["COLLECTOR", "BUYER", "SELLER", "VERIFIER"]
            }
         ' http://backend.collectid.io/api/v1/user/1/roles

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **txHash** [string] - A transaction hash identifier that represents the transaction made to the smart contract.

Example response:
::

    {
        "txHash": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3d"
    }

.. NOTE::
    If the request does not require updates to the smart contract roles, the reponse will NOT include a transaction has. Instead, it will simply return a 200 status. Any change to the "VERIFIER", "ADMIN", or "OWNER" role will require a smart contract transaction.


PUT user/{id}/username
----------------------

Update the username of a specific user.

Role Required
~~~~~~~~~~~~~~

edit:users

Parameters
~~~~~~~~~~

- **id** [integer] - The unique identifier for a specific user to be updated.
- **username** [string] - The username of the user.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X PUT \
         -d '
            {
                "username": "hughmann2019"
            }
         ' http://backend.collectid.io/api/v1/user/1/username

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **id** [integer] - The unique identifier for the created user.

Example response:
::

    {
        "id": 1
    }


GET user/{id}
-------------

Return the details of a specific user.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **id** [integer] - The unique identifier for a specific user to be returned.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/user/1

Response
~~~~~~~~

Returns a JSON object representing a single user object.

Example response:
::

    {
        "id": 1
        "role": ["COLLECTOR", "BUYER", "SELLER"],
        "username": "hughmann2019",
        "address": "0x0000000000000000000000000000000C011EC71D",
        "publicKey": "0x00000000000000...00000000000000C011EC71D"
    }


GET user/{externalId}/external
------------------------------

Return the details of a specific user given their external ID.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **externalId** [string] - The external identifier for the user provided by the authenticity server.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/user/abcd1234/externalId

Response
~~~~~~~~

Returns a JSON object representing a single user object.

Example response:
::

    {
        "id": 1
        "role": ["COLLECTOR", "BUYER", "SELLER"],
        "username": "hughmann2019",
        "address": "0x0000000000000000000000000000000C011EC71D",
        "publicKey": "0x00000000000000...00000000000000C011EC71D"
    }


GET user/{username}/username
----------------------------

Return the details of a specific user given their username.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **username** [string] - The username for the user.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/user/hughmann2019/externalId

Response
~~~~~~~~

Returns a JSON object representing a single user object.

Example response:
::

    {
        "id": 1
        "role": ["COLLECTOR", "BUYER", "SELLER"],
        "username": "hughmann2019",
        "address": "0x0000000000000000000000000000000C011EC71D",
        "publicKey": "0x00000000000000...00000000000000C011EC71D"
    }


GET user/{id}/publickey
-----------------------

Return the public key details of a specific user given their ID.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **id** [integer] - The unique identifier for a specific user for which to return public key data.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/user/1/publickey

Response
~~~~~~~~

Returns a JSON object representing a single public key object.

Example response:
::

    {
        "address": "0x0000000000000000000000000000000C011EC71D",
        "publicKey": "0x00000000000000...00000000000000C011EC71D"
    }


GET user/{id}/collection
------------------------

Return a specific user's collection details.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **id** [integer] - The unique identifier for the specific user whose collection is to be returned.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/user/1/collection

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **collection** [array] - An array of collectible objects.

Example response:
::

    {
        "collection": [
            {
                "id": "0045E54BAB35C82",
                "token_id": 0,
                "product_id": 10,
                "status": "COMMISSIONED",
                "registered": true,
                "verified": true,
                "owner": 1,
                "description": {
                    "brand": "Nike",
                    "model": "Air Jordan I Prototype",
                    "size": "13",
                    "color": "White",
                    "condition": "USED",
                    "brand_logo": "https://collectid.io/api/v1/nike_logo.svg",
                    "detail": "Worn in-game by Michael Jordan himself."
                },
                "images" : [
                    "https://collectid.io/api/v1/example01.jpg",
                    "https://collectid.io/api/v1/example02.jpg",
                    "https://collectid.io/api/v1/example03.jpg"
                ],
                "average_price": 123.45,
                "verifier_id": 42,
                "verifier_name": "Sneakerness",
                "created_at": "2018-09-12T00:00:00.000Z",
                "updated_at": "2018-09-12T00:00:00.000Z"
            },
            ...
            {
                "id": "0045E54BAB35C83",
                "token_id": 1,
                "product_id": 15,
                "status": "COMMISSIONED",
                "registered": true,
                "verified": true,
                "owner": 1,
                "description": {
                    "brand": "Nike",
                    "model": "Air Jordan 3 Retro",
                    "size": "11",
                    "color": "Blue",
                    "condition": "NEW",
                    "brand_logo": "https://collectid.io/api/v1/nike_logo.svg",
                    "detail": ""
                },
                "images" : [
                    "https://collectid.io/api/v1/example04.jpg",
                    "https://collectid.io/api/v1/example05.jpg"
                ],
                "average_price": 75.86,
                "verifier_id": 42,
                "verifier_name": "Sneakerness",
                "created_at": "2018-09-12T00:00:00.000Z",
                "updated_at": "2018-09-12T00:00:00.000Z"
            }
        ]
    }


.. _cURL: https://curl.haxx.se/