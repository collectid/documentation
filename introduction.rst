Introduction
============

This documentation details proper usage of the CollectID MVP RESTful API. This API is designed to be consumed by the CollectID desktop and mobile applications and provides an interface to the blockchain instracture on which the platform is built.

The API provides endpoints for tag generation, user and collectible creation, transfer functionality, and general platform management.