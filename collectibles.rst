Collectibles
============

Collectibles are the main asset of the CollectID ecosystem. Their state is maintained by a smart contract with which the backend server interacts with on behalf of the end user. Collectible objects have the following properties:

- **id** [string] - The tag identifier associated with the collectible.
- **token_id** [integer] - The smart contract identifier associated with the collectible.
- **product_id** [integer] - The product identifier associated with the collectible.
- **status** [string] - The current status of the collectible.
- **registered** [boolean] - Indicates whether or not the collectible has been registered to a collector.
- **verified** [boolean] - Indicates whether or not the current ownership information is verified.
- **owner** [integer] - The user ID of the current owner of the collectible.
- **description** [object] - A JSON object containing details of the collectible.
- **images** [string array] - An array of image URLs that represent the collectible.
- **average_price** [number] - The average sale price of the collectible.
- **verifier_id** [integer] - The user ID of the account which created/authenticated the collectible.
- **verifier_name** [string] - The name of the user account which created/authenticated the collectible.
- **created_at** [string] - A timestamp indicating when the collectible was created.
- **updated_at** [string] - A timestamp indicating the last time the collectible was updated.

A description object has the following properties:

- **brand** [string] - The brand of the collectible.
- **brand_detail** [string] - The details of the brand.
- **brand_logo** [string] - A URL pointing to a brand logo image.
- **model** [string] - The model of the collectible.
- **size** [string] - The size of the collectible.
- **color** [string] - The color of the collectible.
- **condition** [string] - The condition of the collectible.
- **detail** [string] - Additional details needed to describe the collectible.

Possible tag statuses include:

- *REQUESTED* - The collectible has been requested, but is not confirmed by the smart contract.
- *COMMISSIONED* - The collectible has been confirmed by the smart contract and is activated for use.
- *DECOMMISSIONED* - The collectible has been deactivated for use.

Example of a collectible object:
::

    {
        "id": "0045E54BAB35C82",
        "token_id": 0,
        "product_id": 10,
        "status": "COMMISSIONED",
        "registered": true,
        "verified": true,
        "owner": 1,
        "description": {
            "brand": "Nike",
            "brand_detail": "Just Do It.",
            "brand_logo": "https://collectid.io/api/v1/nike_logo.png",
            "model": "Air Jordan I Prototype",
            "size": "13",
            "color": "White",
            "condition": "USED",
            "detail": "Worn in-game by Michael Jordan himself."
        },
        "images" : [
            "https://collectid.io/api/v1/example01.jpg",
            "https://collectid.io/api/v1/example02.jpg",
            "https://collectid.io/api/v1/example03.jpg"
        ],
        "average_price": 123.45,
        "verifier_id": 42,
        "verifier_name": "Sneakerness",
        "created_at": "2018-09-12T00:00:00.000Z",
        "updated_at": "2018-09-12T00:00:00.000Z"
    }


POST collectible
----------------

Commission/create a new collectible.

Role Required
~~~~~~~~~~~~~~

verifier

Parameters
~~~~~~~~~~

- **callerId** [integer] - The user ID of the user making the request (the user must have the "VERIFIER" role).
- **productId** [integer] - The product ID that represents the collectible.
- **tagId** [string] - The tag ID to be associated with the collectible.
- **size** [string] - The size of the collectible.
- **condition** [string] - The condition of the collectible (only "USED" or "NEW" are valid conditions).
- **detail** [string] - Additional details needed to describe the collectible.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "callerId": 42,
                "productId": 10,
                "tagId": "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
                "size": "13",
                "condition": "USED",
                "detail": "Worn in-game by Michael Jordan himself."
            }
         ' http://backend.collectid.io/api/v1/collectible

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **txHash** [string] - A transaction hash identifier that represents the transaction made to the smart contract.

Example response:
::

    {
        "txHash": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3c"
    }

.. NOTE::
    The collectible will not have a *COMMISSIONED* status until the transaction is confirmed within the smart contract. Until then, it will have a *REQUESTED* status.


DELETE collectible/{tagId}
--------------------------

Decommission an existing tag (and associated collectible, if there is one).

Role Required
~~~~~~~~~~~~~~

admin

Parameters
~~~~~~~~~~

- **tagId** - The tag ID of the collectible to be decommissioned.

Example request using cURL_:
::
    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X DELETE \
         ' http://backend.collectid.io/api/v1/collectible/0045E54BAB35C82

Response
~~~~~~~~

A succesful call to this endpoint will simply return a 200 status.


GET collectible
---------------

Retrieve the details of all collectibles in the ecosystem.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/collectible

Role Required
~~~~~~~~~~~~~~

none

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **collectibles** [array] - An array of collectible objects.

Example response:
::

    {
        "collectibles": [
            {
                "id": "0045E54BAB35C82",
                "token_id": 0,
                "product_id": 10,
                "status": "COMMISSIONED",
                "registered": true,
                "verified": true,
                "owner": 1,
                "description": {
                    "brand": "Nike",
                    "brand_detail": "Just Do It.",
                    "brand_logo": "https://collectid.io/api/v1/nike_logo.png",
                    "model": "Air Jordan I Prototype",
                    "size": "13",
                    "color": "White",
                    "condition": "USED",
                    "detail": "Worn in-game by Michael Jordan himself."
                },
                "images" : [
                    "https://collectid.io/api/v1/example01.jpg",
                    "https://collectid.io/api/v1/example02.jpg",
                    "https://collectid.io/api/v1/example03.jpg"
                ],
                "average_price": 123.45,
                "verifier_id": 42,
                "verifier_name": "Sneakerness",
                "created_at": "2018-09-12T00:00:00.000Z",
                "updated_at": "2018-09-12T00:00:00.000Z"
            },
            ...
            {
                "id": "0045E54BAB35C83",
                "token_id": 1,
                "product_id": 15,
                "status": "COMMISSIONED",
                "registered": false,
                "verified": true,
                "owner": 42,
                "description": {
                    "brand": "Nike",
                    "brand_detail": "Just Do It.",
                    "brand_logo": "https://collectid.io/api/v1/nike_logo.png",
                    "model": "Air Jordan 3 Retro",
                    "size": "11",
                    "color": "Blue",
                    "condition": "NEW",
                    "detail": ""
                },
                "images" : [
                    "https://collectid.io/api/v1/example04.jpg",
                    "https://collectid.io/api/v1/example05.jpg"
                ],
                "average_price": 75.86,
                "verifier_id": 42,
                "verifier_name": "Sneakerness",
                "created_at": "2018-09-12T00:00:00.000Z",
                "updated_at": "2018-09-12T00:00:00.000Z"
            }
        ]
    }


GET collectible/{tagId}
-----------------------

Return the details of a specific collectible.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **tagId** - The tag ID of the collectible to be returned.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/collectible/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa

Response
~~~~~~~~

Returns a JSON object representing a single collectible object.

Example response:
:: 

    {
        "id": "0045E54BAB35C82",
        "token_id": 0,
        "product_id": 10,
        "status": "COMMISSIONED",
        "registered": true,
        "verified": true,
        "owner": 1,
        "description": {
            "brand": "Nike",
            "brand_detail": "Just Do It.",
            "brand_logo": "https://collectid.io/api/v1/nike_logo.png",
            "model": "Air Jordan I Prototype",
            "size": "13",
            "color": "White",
            "condition": "USED",
            "detail": "Worn in-game by Michael Jordan himself."
        },
        "images" : [
            "https://collectid.io/api/v1/example01.jpg",
            "https://collectid.io/api/v1/example02.jpg",
            "https://collectid.io/api/v1/example03.jpg"
        ],
        "average_price": 123.45,
        "verifier_id": 42,
        "verifier_name": "Sneakerness",
        "created_at": "2018-09-12T00:00:00.000Z",
        "updated_at": "2018-09-12T00:00:00.000Z"
    }


GET collectible/{tagId}/history
-------------------------------

Return the transfer history of a specific collectible.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **tagId** - The tag ID of the collectible to be returned.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/collectible/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/history

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **history** [array] - An array of event objects.

An event object has the following properties:

- **id** [integer] - A unique ID for the event. IDs are incremental, so smaller IDs occurred earlier in time.
- **created_at** [string] - The time the event occured.
- **event_type** [string] - A string representation of the event.
- **user_from** [integer] - The user ID from which the event initiated.
- **user_to** [integer] - The user ID to which the event is targeted.
- **user_from_name** [string] - The name of the user from which the event initiated.
- **user_to_name** [string] - The name of the user to which the event is targeted.
- **tag_id** [string] - The tag ID of the collectible associated with the event.
- **eth_tx** [string] - The blockchain transaction hash associated with the event.
- **block** [big integer] - The block height of the blockchain transaction.

.. NOTE::
    The first transfer event will have a null 'user_from' field. This is the transfer event logged from a commission/authentication transaction and can be considered the authentication event. In this case, the 'user_from_name' field will be undefined.

Example response:
::

    {
        "history": [
            {
                "id": 13,
                "created_at": "2018-09-12T00:00:00.000Z",
                "event_type": "TRANSFER_SUCCESS",
                "user_from": null,
                "user_to": 1,
                "user_to_name": "Sneakerness",
                "tag_id": "0045E54BAB35C82",
                "eth_tx": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3e",
                "block": 6717374
            },
            ...
            {
                "id": 17,
                "created_at": "2018-09-13T00:00:00.000Z",
                "event_type": "TRANSFER_SUCCESS",
                "user_from": 1,
                "user_to": 3,
                "user_from_name": "Sneakerness",
                "user_to_name": "Hugh",
                "tag_id": "0045E54BAB35C82",
                "eth_tx": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3f",
                "block": 6717375
            }
        ]
    }


PUT collectible/{tagId}/image
-----------------------------

Update the image associated with a specific collectible.

Role Required
~~~~~~~~~~~~~~

verifier

Parameters
~~~~~~~~~~

- **tagId** [string] - The ID of the collectible to be updated.
- **url** [string] - The URL of the image to be added to the collectible object.

Example request using cURL_:
::
    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "url": "http://backend.collectid.io/images/products/0045E54BAB35C82.jpg"
            }
         ' http://backend.collectid.io/api/v1/collectible/0045E54BAB35C82/image

Response
~~~~~~~~

A succesful call to this endpoint will simply return a 200 status.


.. _cURL: https://curl.haxx.se/