Products
========

Product objects are essentially templates of collectible objects. They exist independent of the smart contract and are not ownable by any individual. Products are mapped to by collectible objects and describe that collectible's basic characteristics.

- **id** [integer] - A unique identifier for the product.
- **category** [string] - The category of the product (e.g. watch, shoe, etc.)
- **brand_id** [integer] - The ID of the brand of the product.
- **model** [string] - The model of the product.
- **color** [string] - The color of the product.
- **last_sale** [integer] - Value in USD of the last sale made for this product on StockX.
- **last_sale_exp** [string] - The expiration timestamp on the "last_sale" value.
- **images** [string array] - An array of image URLs that represent the product.
- **creatorId** [integer] - The ID of the user that added the product to the ecosystem.
- **createdAt** [string] - A timestamp indicating when the product was created.
- **updatedAt** [string] - A timestamp indicating when the product was last updated.

Example of a product object:
::
    {
        "id": 10,
        "category": "Shoe",
        "brand_id": 1,
        "model": "Air Presto Off-White",
        "color": "Black",
        "last_sale": 100,
        "last_sale_exp": "1558388019",
        "images": [
            "http://backend.collectid.io/images/products/10_1_fullsize.jpg",
            "http://backend.collectid.io/images/products/10_1_thumbnail.jpg"
        ],
        "creatorId": 42,
        "createdAt": "2018-09-12T00:00:00.000Z",
        "updatedAt": "2018-09-12T00:00:00.000Z"
    }


POST product
------------

Create a new product.

Role Required
~~~~~~~~~~~~~~

create:product

Parameters
~~~~~~~~~~

- **creatorId** [integer] - The ID of the user creating the product.
- **description** [object] - A JSON object containing details of the product.
- **images** [string array] - An array of image URLs that represent the product.

A description object has the following properties:

- **category** [string] - The category of the product (e.g. watch, shoe, etc.)
- **brand** [integer] - The ID of the brand of the product.
- **model** [string] - The model of the product.
- **color** [string] - The color of the product.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "creatorId": 42,
                    "category": "SHOE",
                    "brand": 1,
                    "model": "Air Presto Off-White",
                    "color": "Black"
                },
                "images": []
            }
         ' http://backend.collectid.io/api/v1/product

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **id** [integer] - A unique identifier for the product.

Example response:
::
    {
        "id": 1
    }


GET product
-----------

Retrieve the details of all products in the ecosystem.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/product

Role Required
~~~~~~~~~~~~~~

read:product

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **products** [array] - An array of product objects.

Example response:
::
    {
        "products": [
            {
                "id": 1,
                "category": "Shoe",
                "brand_id": 1,
                "model": "Air Presto Off-White",
                "color": "Black",
                "last_sale": 100,
                "last_sale_exp": "1558388019",
                "images": [
                    "http://backend.collectid.io/images/products/1_1_fullsize.jpg",
                    "http://backend.collectid.io/images/products/1_1_thumbnail.jpg"
                ],
                "creatorId": 42,
                "createdAt": "2018-09-12T00:00:00.000Z",
                "updatedAt": "2018-09-12T00:00:00.000Z"
            },
            ...
            {
                "id": 5,
                "category": "Shoe",
                "brand_id": 1,
                "model": "SB Stefan Janoski Max",
                "color": "Team Red"
                "last_sale": 80,
                "last_sale_exp": "1558388019",
                },
                "images": [
                    "http://backend.collectid.io/images/products/5_1_fullsize.jpg",
                    "http://backend.collectid.io/images/products/5_1_thumbnail.jpg"
                ],
                "creatorId": 42,
                "createdAt": "2018-09-12T00:00:00.000Z",
                "updatedAt": "2018-09-12T00:00:00.000Z"
            }
        ]
    }


GET product/{productId}
-----------------------

Retrieve the details of a specific product by ID.

Role Required
~~~~~~~~~~~~~~

read:product

Parameters
~~~~~~~~~~

- **productId** - The ID of the product to be returned.

Example request using cURL_:
::

    curl -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         http://backend.collectid.io/api/v1/product/1

Response
~~~~~~~~

Returns a JSON object representing a single product object.

Example response:
::
    {
        "id": 1,
        "category": "Shoe",
        "brand_id": 1,
        "model": "Air Presto Off-White",
        "color": "Black",
        "last_sale": 100,
        "last_sale_exp": "1558388019",
        "images": [
            "http://backend.collectid.io/images/products/1_1_fullsize.jpg",
            "http://backend.collectid.io/images/products/1_1_thumbnail.jpg"
        ],
        "creatorId": 42,
        "createdAt": "2018-09-12T00:00:00.000Z",
        "updatedAt": "2018-09-12T00:00:00.000Z"
    }


PUT product/{productId}/image
-----------------------------

Update the images associated with a specific product.

Role Required
~~~~~~~~~~~~~~

verifier

Parameters
~~~~~~~~~~

- **productId** [integer] - The ID of the product to be updated.
- **urls** [string array] - The URL of the image to be added to the product object.

Example request using cURL_:
::
    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "urls": [
                    "http://backend.collectid.io/images/products/1_2_fullsize.jpg",
                    "http://backend.collectid.io/images/products/1_2_thumbnail.jpg",
                ]
            }
         ' http://backend.collectid.io/api/v1/product/1/image

Response
~~~~~~~~

A succesful call to this endpoint will simply return a 200 status.


.. _cURL: https://curl.haxx.se/