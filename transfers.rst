Transfers
=========

Transfers are the primary means of modifying the ownership of collectibles within the CollectID ecosystem. Every *transfer* and *registration* operation creates a transaction within the smart contract, so each of these requests will return a transaction hash object:
::

    {
        "txHash": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3d"
    }

Transactions will not complete instantly, so it's important to check that the previous transaction is complete before another one is made for the same collectible. Otherwise, unexpected transaction failures are likely to occur.


POST collectible/transfer
-------------------------

Create a new transfer transaction.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **from** [integer] - The ID of the user transfering the collectible. This user must be the owner of the collectible.
- **to** [integer] - The ID of the user receiving the collectible.
- **tag** [string] - The contents of the scanned tag.
- **signedHash** [string] - A transaction hash signed by the "from" user's private key.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "from": 1,
                "to": 2,
                "tag": "0045E54BAB35C82",
                "signedHash": "0x80aeb89988cc89e01...c350d67087ff683df26d99f1"
            }
         ' http://backend.collectid.io/api/v1/collectible/transfer

Details
~~~~~~~

The "signedHash" object is an Ethereum signed message of the following format:

"\x19Ethereum Signed Message:\n" + "32" (size of the message in bytes) + SHA3-hashed ABI-encoded parameters sent to the smart contract

The ABI-encoded parameters sent to the smart contract are as follows:

::

    {
        ["string", "uint256", "address"],
        ["Transfer", token_id, receiver_addres]
    }

Example of a function that creates a properly formatted "signedHash" object:

::

    const abi = require('ethereumjs-abi');
    const util = require('ethereumjs-util');

    function signTransferMessage(tokenId, receiverAddress, signerPrivateKey) {

        // Encode the typed data to be sent
        const encodedData = abi.rawEncode(
            ["string", "uint256", "address"],
            ["Transfer", tokenId, receiverAddress]
        );

        // Format and hash the message
        const msgHash = util.keccak256(Buffer.concat([
            new Buffer("\x19Ethereum Signed Message:\n32"),
            util.keccak256(encodedData)
        ]));

        // Sign message with senders private key
        const sig = util.ecsign(msgHash, util.toBuffer(signerPrivateKey));

        // Return the RPC formatted signature
        return util.toRpcSig(sig.v, sig.r, sig.s);
    }

Providing the following inputs to the function above will return the following outputs:

| Inputs:  
| tokenId = 0  
| receiverAddress = "0x0000000000000000000000000000000C0113C71D"  
| signerPrivateKey = "0x0000000000000000000000000000000000000000000000000000000C0113C71D"  

| Output:  
| "0x9c1349d9fd951e42b209989e320b36f755c03dfbfc7f35a2417473a9f63a4173343460b71b1783feb3af0ad4d37ce372a9d8d541027d710c355230999bfd0cb01c"

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **txHash** [string] - A transaction hash identifier that represents the transaction made to the smart contract.

Example response:
::

    {
        "txHash": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3d"
    }


POST collectible/register
-------------------------

Register a new collectible to a user.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **to** [integer] - The ID of the user registering the collectible.
- **tag** [string] - The contents of the scanned tag.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "to": 1,
                "tag": "0045E54BAB35C82"
            }
         ' http://backend.collectid.io/api/v1/collectible/register

Response
~~~~~~~~

Returns a JSON object containing the following properties:

- **txHash** [string] - A transaction hash identifier that represents the transaction made to the smart contract.

Example response:
::

    {
        "txHash": "0x10c17f9087b487c153d34c381f11ef5c268c50be5c47e4bb75dd426a39ac1b3e"
    }


POST collectible/verify
-----------------------

Verify physical ownership of a newly transferred collectible.

Role Required
~~~~~~~~~~~~~~

none

Parameters
~~~~~~~~~~

- **to** [integer] - The user ID of the user targeted to receive the collectible (usually the active user).
- **tag** [string] - The contents of the scanned tag.

Example request using cURL_:
::

    curl -H "Content-Type: application/json" \
         -H "Authorization: Bearer aaaabbbbcccc...111122223333" \
         -X POST \
         -d '
            {
                "to": 2,
                "tag": "0045E54BAB35C82"
            }
         ' http://backend.collectid.io/api/v1/collectible/verify

Response
~~~~~~~~

A succesful call to this endpoint will simply return a 200 status.


.. _cURL: https://curl.haxx.se/